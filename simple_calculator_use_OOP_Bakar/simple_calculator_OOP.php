<?php
    
    include_once './classes/calculator.php';
    $obj=new Calculator();
    
    ?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Simple Calculator Use OOP</title>
</head>
<body>
	<form action="simple_calculator_OOP.php" method="post">
    	<table>
        	<tr>
            	<td>First Number:</td>
                <td>
                	<input type="number" name="first_number" value="<?php echo $_POST['first_number']; ?>" />
                </td>
            </tr>
            <tr>
            	<td>Second Number:</td>
                <td>
                	<input type="number" name="second_number" value="<?php echo $_POST['second_number']; ?>" />
                </td>
            </tr>
            <tr>
            	<td></td>
                <td>
                    <input type="submit" name="btn" value="+" />
                    <input type="submit" name="btn" value="-" />
                    <input type="submit" name="btn" value="*" />
                    <input type="submit" name="btn" value="/" />
                    <input type="submit" name="btn" value="%" />
                </td>
            </tr>
            <tr>
            	<td>Result:</td>
                <td>
					  
                </td>
            </tr>
                <?php 
                    if(isset($_POST['btn']))
                    {
                        $first_number=$_POST['first_number'];
                        $second_number=$_POST['second_number'];

                        if ($_POST['btn']=='+')
                        {
                            $info=$obj->addition($first_number,$second_number);
                        }

                        if ($_POST['btn']=='-')
                        {
                            $info=$obj->subtruction($first_number,$second_number);
                        }

                        if ($_POST['btn']=='*')
                        {
                            $info=$obj->multiplication($first_number,$second_number);
                        }

                        if ($_POST['btn']=='/')
                        {
                            $info=$obj->divition($first_number,$second_number);
                        }

                        if ($_POST['btn']=='%')
                        {
                            $info=$obj->reminder($first_number,$second_number);
                        }
                                    
                    }
                ?>
            <tr>
            	<td><?php echo $info['lbl'];?></td>
                <td><?php echo $info['result'];?></td>
            </tr>
        </table>
    </form>


</body>
</html>