<?php

require_once '../../../../Src/Bitm/Seip10/registration/Registration.php';

use RegApp\Bitm\Seip10\registration\Registration;


$objRegister = new Registration();


$objRegister ->prepare($_GET)-> register();



?>

<html lang="en">
<head>
  <title>Registration</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="../css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

    <div class="container" style="margin-top:5%;">
        <div class="row">
        
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Varification !</strong></h3>
                </div>

                <div class="panel-body">
                    <div class="alert alert-info" role="alert">
                        <a href="login.php" class="btn btn-info btn-lg btn-block"><p><strong><?php $objRegister ->warningMsg('alreadyValid');$objRegister ->warningMsg('validSuccess'); ?>.</strong></p></a>
                    </div>
                </div>
            </div>


        </div><!--row-->
    </div><!--container-->

</body>