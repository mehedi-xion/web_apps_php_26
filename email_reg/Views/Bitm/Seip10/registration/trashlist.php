<?php

require_once '../../../../Src/Bitm/Seip10/registration/Registration.php';

use RegApp\Bitm\Seip10\registration\Registration;

$objtrashlist = new Registration();

$data = $objtrashlist ->alluser();

if(!empty($_SESSION['user']) && isset($_SESSION['user'])) {
    
    if( $_SESSION['user']['is_admin'] ==1){
        $i = 1; $serial = 0;
        ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Trash</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="../css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<div class="container" style="margin-top:5%;">
    <div class="row">
        
        <?php $objtrashlist->warningMsg('deleteSuccess'); ?>
        
        <div class="panel panel-success">
           <table class="table table-bordered">
            <div class="panel-heading">
                 <h2 class="center-block" align="center">Trash Data of User</h2>
                <tr class="success">
                  <th>ID</th>
                  <th>Username</th>
                  <th>Password</th>
                  <th>Email</th>
                  <th>AdminShip</th>
                  <th>Varified</th>
                  <th>Created</th>
                  <th>Modified</th>
                  <th>Deleted</th>
                  <th colspan="2" style="text-align: center;">Action</th>
                </tr>
            </div>
               <div class="panel-body">
                   
                       <tbody>
                            <?php
                            foreach($data as $singleData){
                                if($singleData['is_deleted'] == 0){
                                    continue;
                                }
                                
                                ?>
                          <tr class="<?php      if($i %2 == 0){
                                                echo "danger";}else{
                                                    echo "info";
                                                }
                                                $i++;  ?>">
                                <td><?php echo $serial++;?></td>
                                <td><?php echo $singleData['username']; ?></td>
                                <td><?php echo $singleData['password']; ?></td>
                                <td><?php echo $singleData['email']; ?></td>
                                <td><?php echo $singleData['is_admin']; ?></td>
                                <td><?php echo $singleData['is_active']; ?></td>
                                <td><?php echo $singleData['created']; ?></td>
                                <td><?php echo $singleData['modified']; ?></td>
                                <td><?php echo $singleData['deleted']; ?></td>
                                
                                <td><a href="restore.php?id=<?php echo $singleData['unique_id']; ?>" class="btn btn-primary">Restore</td>
                                <td><a href="delete.php?id=<?php echo $singleData['unique_id']; ?>" class="btn btn-primary">Parmenet Delete</td>

                          </tr>
                          <?php 
                            }
                            ?>
                    </tbody>
                  </table>
            
               </div>
               <a href="list.php">Want to see list</a>
   
               
               <?php include 'footermenu.php'; ?>
    </div><!--row-->
</div><!--container-->


  
</body>
</html>
<?php
    } else{
        $_SESSION['errorMsg'] = "This page is only for ";
         header("location:error.php");
    }

        

    
}else{
    
    $_SESSION['errorMsg'] = "Sorry ! You don't have the permission to access";
    header("location:error.php");
}
        
        
?>