<?php

require_once '../../../../Src/Bitm/Seip10/registration/Registration.php';

use RegApp\Bitm\Seip10\registration\Registration;


$objProfile = new Registration();

if(isset($_GET['id'])){
    $userData = $objProfile ->prepare($_GET)-> singleUser();
}else{
    
    $_GET['id'] = $_SESSION['user']['unique_id'];
    $userData = $objProfile ->prepare($_GET)-> singleUser();
}


if(!empty($userData['birthdate'])){
$bday = unserialize($userData['birthdate']);}

if(!empty($userData['interest'])){
$interest = unserialize($userData['interest']);}

if(!empty($userData['languages'])){
$language = unserialize($userData['languages']);}

$address = unserialize($userData['address']);


//if(!empty($userData) && !isset($userData)){
    
//    if(!empty($userData) && isset($userData)) {

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Profile</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <link href="../css/style.css" rel="stylesheet">
  <link href="http://fontawesome.io/assets/font-awesome/css/font-awesome.css" rel="stylesheet" media="screen">  
</head>
<body>

    <div class="container" style="margin-top:5%;">
        <div class="row">
        <div class="user-menu-container square">
           <div class="col-md-6 user-details">
            <div class="row coralbg white">
                <div class="col-md-6 no-pad">
                    <div class="user-pad">
                        <h3 style="font-family: Nimbus Mono L"><b><?php echo $userData['full_name']; ?></b></h3>
                        <h4 class="white"><i class="fa fa-male"></i> <?php echo $userData['gender']; ?></h4>
                        <h4 class="white"><i class="fa fa-birthday-cake"></i><?php
                        if(!empty($bday)){
                            foreach($bday as $value){
                                echo $value." ";
                            }
                        }
                        ?></h4>
                        <h4 class="white"><i class="fa fa-mobile"></i> <?php echo $userData['mobile']; ?></h4>
                        <h4 class="white"><i class="fa fa-map-marker"></i><?php
                        if(!empty($address)){
                            foreach($address as $value){
                                echo $value." ";
                            }
                        }
                        ?></h4>
                        <a type="button" class="btn btn-success btn-lg"" href="edit.php?id=<?php echo $userData['unique_id'];?>">
                            <span class="btn-label"><i class="fa fa-pencil"></i></span>Update Profile
                        </a>
                    </div>
                </div>
                <div class="col-md-6 no-pad">
                    <div class="user-image">
                        <img src="../img/user3.jpg" class="img-responsive thumbnail">
                    </div>
                </div>
            </div>
            <div class="row overview">
                <div class="col-md-6 user-pad text-center">
                    <h3>CREATE DATE</h3>
                    <h4><?php echo $userData['created']; ?></h4>
                </div>
                <div class="col-md-6 user-pad text-center">
                    <h3>LAST MODIFIED</h3>
                    <h4><?php echo $userData['modified']; ?></h4>
                </div>
            </div>
        </div>
        <div class="col-md-1 user-menu-btns">
            <div class="btn-group-vertical square" id="responsive">
                <a href="#" class="btn btn-block btn-default active">
                    <i class="fa fa-info"></i><p>About</p>
                </a>
                <a href="#" class="btn btn-default">
                  <i class="fa fa-briefcase"></i><p>Academic</p>
                </a>
                <a href="#" class="btn btn-default">
                  <i class="fa fa-user"></i><p>Personal</p>
                </a>
                <a href="#" class="btn btn-default">
                  <i class="fa fa-envelope"></i><p>Contact</p>
                </a>
            </div>
        </div>
        <div class="col-md-4 user-menu user-pad">
            <div class="user-menu-content active">
                <h3>
                    About Myself
                </h3>
                <ul class="user-menu-list">
                    <li>
                        <h4><i class="fa fa-user coral"></i>Name: : <?php echo $userData['full_name']; ?></h4>
                    </li>
                    <li>
                        <h4><i class="fa fa-heart-o coral"></i> Maritial Status : <?php echo $userData['marritial_stat']; ?></h4>
                    </li>
                    <li>
                        <h4><i class="fa fa-medkit coral"></i>Blood Group : <?php echo $userData['blood']; ?></h4>
                    </li>
                    <li>
                        <h4><i class="fa fa-flag coral"></i> Nationality : <?php echo $userData['Nationality']; ?></h4>
                    </li>
                </ul>
            </div>
            <div class="user-menu-content">
                <h3>
                    Academic Info
                </h3>
                <ul class="user-menu-list">
                    <li>
                        <h4>Education : <?php echo $userData['education']; ?></h4>
                    </li>
                    <li>
                        <h4>Occupation : <?php echo $userData['occupation']; ?></h4>
                    </li>
                    <li>
                        <h4>Job Position : <?php echo $userData['job_status']; ?></h4>
                    </li>
                    <li>
                        <h4>Skills Area : <?php echo $userData['skill_arel']; ?></h4>
                    </li>
                    <li>
                        <h4>Bio : <?php echo $userData['bio']; ?></h4>
                    </li>
                </ul>
            </div>
            <div class="user-menu-content">
                <h3>
                    Personal Info
                </h3>
                <ul class="user-menu-list">
                    <li>
                        <h4>Father Name <?php echo $userData['father_name']; ?></h4>
                    </li>
                    <li>
                        <h4>Mother Name : <?php echo $userData['mother_name']; ?></h4>
                    </li>
                    <li>
                        <h4>Interest : <?php if(!empty($interest)){
                            foreach($interest as $value){
                                echo $value." ";
                            }
                        }
                        ?></h4>
                    </li>
                    <li>
                        <h4>Languages : <?php if(!empty($language)){
                            foreach($language as $value){
                                echo $value." ";
                            }
                        }
                        ?></h4>
                    </li>
                    <li>
                        <h4>Religion : <?php echo $userData['religion']; ?></h4>
                    </li>
                </ul>
            </div>
            <div class="user-menu-content">
                <h3>
                    Contact Info
                </h3>
                <ul class="user-menu-list">
                    <li>
                        <h4>Mobile Number <?php echo $userData['mobile']; ?></h4>
                    </li>
                    <li>
                        <h4>National ID : <?php echo $userData['nid']; ?></h4>
                    </li>
                    <li>
                        <h4>Passport Number : <?php echo $userData['passpoet']; ?></h4>
                    </li>
                    <li>
                        <h4>Fax Number : <?php echo $userData['fax']; ?></h4>
                    </li>
                </ul>
            </div>
        </div>
       </div>
       </div> <!--- ROW -->
            <div class="row">
                
                 <?php
                            if( $userData['is_admin'] ==1){
                                ?>
                        
                        <div class="btn-group btn-group-justified" role="group">
                            
                            <a class="btn btn-success btn-lg btn-block" role="button" href="edit.php?id=<?php echo $userData['unique_id'];?>">Edit</a>
                            <a class="btn btn-success btn-lg btn-block" role="button" href="delete.php?id=<?php echo $userData['unique_id'];?>">Delete</a>
                            <a class="btn btn-success btn-lg btn-block" role="button" href="photo.php?id=<?php echo $userData['unique_id'];?>">Upload Photo</a>
                            <a class="btn btn-success btn-lg btn-block" role="button" href="list.php">User list</a>
                            
                            
                        </div><!--btn group-->
                        
                        <?php
                        
                            }else{
                                ?>
                                <a class="btn btn-warning btn-lg btn-block" role="button" href="photo.php?id=<?php echo $userData['unique_id'];?>">Upload Profile Pic</a>
                                <?php
                            }
                        
                        ?>
                                
                      <br><a href="logout.php"><button type="button" class="btn btn-danger btn-lg btn-block"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span>Logout</button></a>  

                    </div><!--2nd Row-->

                <?php

                
//            }else{
//                $_SESSION['regNotyet'] = '<p class="text-danger">Sorry, Please login first to see the profile.</p>';
//                        header('location:login.php');
//            }
            
            include 'footermenu.php';
    
//}else{
//    $_SESSION['errorMsg'] = "Sorry Wrong URL ";
//    header("location:error.php");
//}

?>
