<?php

require_once '../../../../Src/Bitm/Seip10/registration/Registration.php';

use RegApp\Bitm\Seip10\registration\Registration;


$objLogin = new Registration();

?>

<html lang="en">
<head>
  <title>Login</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="../css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

    <div class="container" style="margin-top:5%;">
    <div class="row">
           <?php $objLogin ->warningMsg('regNotyet'); ?>
           <?php $objLogin ->warningMsg('emptyField'); ?>
        <fieldset>
          
        <legend class="text-center"><h2>Please Login <span class="glyphicon glyphicon-log-in"></span> Here</h2></legend>

        <form class="center-block" method="POST" action="loginaction.php">
                <div class="form-group">
                  <label for="exampleInputEmail1">User name</label>
                  <input type="text" class="form-control" name="uname" placeholder="Username" value="<?php $objLogin ->warningMsg('usernameShow');?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <input type="password" class="form-control" name="passwrd" placeholder="Password">
                </div>
                <button type="submit" class="btn btn-default">Login</button>
            </form>
        </fieldset>
         <br>
        <a href="create.php" class="btn btn-warning btn-lg btn-block">Click Here for Register if you doesn't have a account</a>
        
        
        <?php include 'footermenu.php'; ?>
    </div><!--row-->
</div><!--container-->


  
</body>
</html>