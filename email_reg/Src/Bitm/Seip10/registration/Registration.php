<?php
namespace RegApp\Bitm\Seip10\registration;

use PDO;
class Registration {
    
    // for PDO
    public $dbusername = "root";
    public $dbpassword  = "";
    public $conn = "";

// for class
    public $id = "";
    public $unique_id = "";
    public $verified_id = "";
    public $user_name = "";
    public $password = "";
    public $confirm_passwrd = "";
    public $email = "";
    public $created = "";
    public $modified = "";
    public $deleted = "";
    public $is_admin = "";
    public $is_active = "";
    public $validate;
    public $listData = "";
    public $is_delete = "";
    
    // for Profile
    
    public $name = "";
    public $father = "";
    public $mother = "";
    public $gender = "";
    public $birthday = "";
    public $occupation = "";
    public $education = "";
    public $religion = "";
    public $married = "";
    public $mobile = "";
    public $job = "";
    public $nation = "";
    public $interest = "";
    public $bio = "";
    public $nid = "";
    public $passport = "";
    public $skill = "";
    public $language = "";
    public $pic = "";
    public $blood = "";
    public $height = "";
    public $fax = "";
    public $address = "";
    public $other = "";
    
    
    public function __construct() {
         session_start();
        $this ->conn = new PDO('mysql:host=localhost;dbname=web_apps_php_26', $this->dbusername, $this->dbpassword);
        
    } // construct \\
    
    
    
    public function prepare($data = ""){
        
        if(!empty($data['uname'])){
            $this->user_name = $data["uname"];
        }
        
        if(!empty($data['passwrd'])){
            $this->password = $data["passwrd"];
        }
        if(!empty($data['passwrd2'])){
            $this->confirm_passwrd = $data["passwrd2"];
        }
        
        if(!empty($data['email'])){
            $this->email = $data["email"];
        }
        
        if(!empty($data['verified_id'])){
            $this->email = $data["verified_id"];
        }
        
        if(!empty($data['id'])){
            $this->id = $data["id"];
        }

        $this->is_admin = 0;
        $this->is_active = 0;
        $this->is_deleted = 0;
        
        
        return $this;
        
    } // prepare
    

    
    
    public function validate(){
        
        // *******************************************************************
        // *******************username validation start here *****************
        // *******************************************************************
        if(!empty($this->user_name)){
//            echo "user not empty<br>";
            if(strlen($this->user_name) >= 6 && strlen($this->user_name) <= 12){
//                    echo "charecter is ok<br>";
                    $query = "SELECT `username` FROM registration WHERE `username`='$this->user_name' ";
                    $sql = $this->conn->prepare($query);
                    $sql ->execute();

                    $result = $sql->fetch(PDO::FETCH_ASSOC);
                    if(empty($result)){
                        
//                      echo "100% OK<br>";
                        $_SESSION['show_username'] = $this->user_name;
                        $this->validate = TRUE;
                        
                    }else{ // if of query for uniqe user
                        
                        $this->validate = FALSE; 
                        
                        $_SESSION['show_username'] = $this->user_name;
                        
                        $_SESSION['unique_user'] = '<p class="text-danger"> <strong>User Exists !</strong> This user name "'.$this->user_name.'" is already taken</p>';
                        
                    }// else of query for uniqe user
                
            }else{ // if of strlen()
                
                $_SESSION['user_char'] = '<p class="text-danger"> <strong>Character length </strong> must be atleast 6 to 12</p>';
                $_SESSION['show_username'] = $this->user_name;
                
                $this->validate = FALSE; 
            }// else of strlen();
            
        }else{ // if of !empty username 
            $_SESSION['user_empty'] = '<p class="text-danger"> <strong>Empty field !</strong>Please fill the username</p>';
            
            $this->validate = FALSE; 
        } // else of !empty \\
        
        // *******************************************************************
        // ***Username validation ends & Password validation start here ******
        // *******************************************************************
        
        if(!empty($this->password)){
            
            if(strlen($this->password) >= 6 && strlen($this->password) <= 12){
                
                $_SESSION['show_password'] = $this->password;
                
            } else { // if strlen for password character validation
                
                $_SESSION['password_char'] = '<p class="text-danger">Passwod must be <strong>6 to 12 charecter</strong> in length</p>';
                
                $this->validate = FALSE;
            }// else strlen() for password character  validation
            
        }else{ // if of !empty password
            
            $_SESSION['password_empty'] = '<p class="text-danger"> <strong>Empty field !</strong> Please give the password</p>';
            
                $this->validate = FALSE;
            
        } // else of !empty password
        
        // *******************************************************************
        // *****Password validation ends & Confirm Password start here *******
        // *******************************************************************
        
        if(!empty($this->confirm_passwrd)){
            if($this->password == $this->confirm_passwrd){
                $this->validate = TRUE;
                
            }else { // if as password match for confirm
                
                $_SESSION['confirm_password_match'] = '<p class="text-danger"> <strong>Password dont match !</strong> Must be match with password</p>';
                
                $this->validate = FALSE;
                
            }// if as password match for confirm
            
        }else{  // if !empty confirm password validation
            
             $_SESSION['confirm_password_empty'] = '<p class="text-danger"> <strong>Empty field !</strong> Please retype the password</p>';
             
             $this->validate = FALSE;
             
        } // else !empty confirm password validation
        
        // *******************************************************************
        // *******Confirm Password ends & Email Validation start here ********
        // *******************************************************************
        
        if(!empty($this->email)){
            
                 $query = "SELECT `email` FROM registration WHERE `email`='$this->email' ";
                
                $stmt = $this->conn -> prepare($query);
                $stmt -> execute();
                
                $result = $stmt->fetch(PDO::FETCH_ASSOC);
                if(empty($result)){

                    $_SESSION['show_email'] = $this->email;
                    $this->validate = TRUE;
                    
                }else{ // 
                    
                     $_SESSION['unique_email'] = '<p class="text-danger"> <strong>Email Exists !</strong> This email is already exits</p>';
                     
                        $this->validate = FALSE;
                } 

        }  else { // if for !empty email validation
            
            $_SESSION['email_empty'] = '<p class="text-danger"> <strong>Empty field !</strong> Please enter your Email address</p>';
             
             $this->validate = FALSE;
             
        } // if for !empty email validation
      
        
    } // // validate \\ \\
    
    
    
    
    
    public function warningMsg($data = ''){
        
        if(!empty($_SESSION["$data"]) && isset($_SESSION["$data"]) ){
            
            echo $_SESSION["$data"];
            unset($_SESSION["$data"]);
        }// if \\
    }// errorMsg
    
    
    
    
    
    
    public function profilePrepare($data = ""){
        
        if(!empty($data['full_name'])){
            $this->name = $data["full_name"];
        }
        
        if(!empty($data['father_name'])){
            $this->father = $data["father_name"];
        }
        
        if(!empty($data['mother_name'])){
            $this->mother = $data["mother_name"];
        }
        if(!empty($data['day']) && !empty($data['month']) && !empty($data['year'])){
            $this->birthday = array( "day"=>$data["day"], "month"=>$data["month"], "year" => $data["year"]);
        }
        
        if(!empty($data['gender'])){
            $this->gender = $data["gender"];
        }
        
        if(!empty($data['occupation'])){
            $this->occupation = $data["occupation"];
        }
        
        if(!empty($data['education'])){
            $this->education = $data["education"];
        }
        
        if(!empty($data['religion'])){
            $this->religion = $data["religion"];
        }
        
        if(!empty($data['marritial_stat'])){
            $this->married = $data["marritial_stat"];
        }
        
        if(!empty($data['job_status'])){
            $this->job = $data["job_status"];
        }
        
        if(!empty($data['Nationality'])){
            $this->nation = $data["Nationality"];
        }
        
        if(!empty($data['bio'])){
            $this->bio = $data["bio"];
        }
        
        if(!empty($data['mobile'])){
            $this->mobile = $data["mobile"];
        }
        
        if(!empty($data['nid'])){
            $this->nid = $data["nid"];
        }
        
        if(!empty($data['passpoet'])){
            $this->passport = $data["passpoet"];
        }
        
        if(!empty($data['fax'])){
            $this->fax = $data["fax"];
        }
        
        if(!empty($data['skill_arel'])){
            $this->skill = $data["skill_arel"];
        }
        
        if(!empty($data['interest'])){
            $this->interest = $data["interest"];
        }
        
        if(!empty($data['languages'])){
            $this->language = $data["languages"];
        }
        
        if(!empty($data['blood']['0'])){
            $this->blood = $data['blood']['0'];
        }
        
        if(!empty($data['height'])){
            $this->height = $data["height"];
        }
        
        if(!empty($data['other'])){
            $this->other = $data["other"];
        }
        
        if(!empty($data['street']) || !empty($data['area']) || !empty($data['city'])){
            $this->address = array('add1'=> $data["add1"],'street'=> $data["street"],'street'=> $data["street"],'city'=> $data["city"],'zip'=> $data["zip"]);
        }
        
        return $this;
        
    }
    
    
    
    
    
    
    
    
    
    function store(){
        if($this->validate === TRUE){

             try{
                 
                 $this->verified_id = uniqid();
                 $this->unique_id = uniqid();
                 
                 $query = "INSERT INTO registration (`id`, `unique_id`, `verified_id`, `username`, `password`, `email`, `is_admin`, `is_active`, `created`, `modified`, `deleted`, `is_deleted`) VALUES (:id, :uniqueid, :verifiedid, :user, :pass, :email, :admin, :active, :creat, :modified, :delete, :is_delete)";
                            
              $stmt = $this->conn->prepare($query);
              $stmt ->execute(array(
                 ":id" =>null,
                  ":uniqueid" => $this->unique_id,
                  ":verifiedid" => $this->verified_id, 
                  ":user" => $this->user_name,          
                  ":pass" => $this->password,          
                  ":email" => $this->email,          
                  ":admin" => $this->is_admin,          
                  ":active" => $this->is_active,          
                  ":creat" => date("Y-m-d h:i:s"),         
                  ":modified" => $this->modified,          
                  ":delete" => $this->deleted,          
                  ":is_delete" => $this->is_delete,          
              ));
              
              $_SESSION['storeMsg'] = '<a href="register.php?id='.$this->unique_id.'" class="btn btn-warning btn-lg btn-block"> <p class="text-success"><strong>Registration Success!</strong>Please click here to  verify your mail.</p></a>';
              
              
              $last_id =  $this->conn ->lastInsertId(); // create as id as profile as user
              
              $queryProfile = "INSERT INTO profile (`id`, `user_id`, `full_name`,`father_name`, `mother_name`, `gender`, `birthdate`, `occupation`, `education`, `religion`, `marritial_stat`, `mobile`, `job_status`, `Nationality`, `interest`, `bio`, `nid`, `passpoet`, `skill_arel`, `languages`, `profile_pic`, `blood`, `height`, `fax`, `address`, `other`, `created`, `modified`, `deleted`) VALUES (:id, :user_id, :full_name, :father_name, :mother_name, :gender, :birthdate, :occupation, :education, :religion, :marritial_stat, :mobile, :job_status, :Nationality, :interest, :bio, :nid, :passpoet, :skill_arel, :languages, :profile_pic, :blood, :height, :fax, :address, :other, :created, :modified, :deleted)";
                            
              $stmt = $this->conn ->prepare($queryProfile);
                            
              $stmt ->execute(array(
                 ":id" =>null,
                 ":user_id" =>$last_id,
                 ":full_name" =>$this->name,
                 ":father_name" =>$this->father,
                 ":mother_name" =>$this->mother,
                 ":gender" =>$this->gender,
                 ":birthdate" =>$this->birthday,
                 ":occupation" =>$this->occupation,
                 ":education" =>$this->education,
                 ":religion" =>$this->religion,
                 ":marritial_stat" =>$this->married,
                 ":mobile" =>$this->mobile,
                 ":job_status" =>$this->job,
                 ":Nationality" =>$this->nation,
                 ":interest" =>$this->interest,
                 ":bio" =>$this->bio,
                 ":nid" =>$this->nid,
                 ":passpoet" =>$this->passport,
                 ":skill_arel" =>$this->skill,
                 ":languages" =>$this->language,
                 ":profile_pic" =>$this->pic,
                 ":blood" =>$this->blood,
                 ":height" =>$this->height,
                 ":fax" =>$this->fax,
                 ":address" =>$this->address,
                 ":other" =>$this->other,
                 ":created" =>date("Y-m-d h:i:s"),
                 ":modified" =>$this->modified,
                 ":deleted" =>$this->deleted,
                ));
                header("location:create.php");
       
            } catch(PDOException $ex){
                echo "Error: ". $ex->getMessage();
            }// catch
           
       }else{// if true;
           header('location:create.php');
       }
    
    }// store \\
    
    
    
    
    
    public function register(){
        
        $unique_id = " '$this->id' ";
        
        $query = "SELECT * FROM `registration` WHERE `unique_id`= $unique_id ";
        
        $stmt = $this->conn -> prepare($query);
        $stmt ->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        
        if(!empty($result['verified_id'])){
            
            if($result['is_active'] == 1){
                
                $_SESSION['alreadyValid'] = "You are already registrated. Please click here for login";
            } // if result == 1
            else if($result['is_active'] == 0){
                try {
                    
                  $query = "UPDATE `registration` SET `is_active` = '1' WHERE `registration`.`verified_id` ='".$result['verified_id']."'";
                    $stmt = $this->conn->prepare($query);
                    if($stmt ->execute()){
                        $_SESSION['validSuccess'] = "Varification successfully completed. Please click here for login";
                    }
                    
                } catch (PDOException $exc) {
                    echo $exc->getTraceAsString();
                }
           } // else if PDO query run
        }else{ // if !empty 
            $_SESSION['errorMsg'] = "You are unauthorized to register";
            header('location:error.php');
        }


        
    }// Register \\
    
    
    
    
    public function login(){
        
        if(!empty($this->user_name)){
            if( !empty($this->password) ){
                                
                $query = "SELECT * FROM `registration` WHERE `username`='".$this->user_name."' AND `password`= '".$this->password."'";
                

                $stmt = $this->conn-> prepare($query);
                $stmt ->execute();

                $result = $stmt->fetch(PDO::FETCH_ASSOC);

                if(isset($result) && !empty($result)){

                    if($result['is_active'] == 1){
                        $_SESSION['user'] = $result;
                        
//                        $joinQuery = "SELECT * FROM `registration`JOIN `profile` ON profile.user_id = registration.id";
                            $joinQuery = "SELECT * FROM `registration` LEFT JOIN `profile` ON `registration`.`id` = `profile`.`user_id`";
                            $stmt = $this->conn->prepare($joinQuery);
                            $stmt ->execute();
                            $SessionData = $stmt->fetch(PDO::FETCH_ASSOC);
                            $_SESSION['joinDb'] = $SessionData;
                        
                        header('location:index.php');

                    }else{

                        $_SESSION['regNotyet'] = '<a href="register.php?id='.$result['unique_id'].'" class="btn btn-warning btn-lg btn-block">Sorry you are not verified yet. Click here for varified</a>';
                        header('location:login.php');

                    } // else is_active == 1

                }else{// if result !empty 

                    $_SESSION['regNotyet'] = '<p class="text-danger">Wrong Username & Password. Please try again</p>';
                        header('location:login.php');
                   }
            }  else {
                $_SESSION['emptyField'] = '<p class="text-danger">Must give Password</p>';
                $_SESSION['usernameShow'] = $this->user_name;
                header('location:login.php'); 
            }
            
        }else{ // if Not empty user name
           $_SESSION['emptyField'] = '<p class="text-danger">Please provide Username</p>';
                        header('location:login.php'); 
        }
        
        
        
    } // Login \\
    
    
    
    
    
    public function alluser(){ // for showing the list data for admin
        
        try{
             $query = "SELECT * FROM `profile` LEFT JOIN `registration` ON `profile`.`user_id` = `registration`.`id` ";
        
            $stmt = $this->conn->prepare($query);
            $stmt ->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){

                $this->listData[] = $row;
            }
            
            return $this->listData;
            
        } catch (Exception $ex) {
            echo 'ERROR: ' . $ex->getMessage();
        }
    
       
    }// alluser \\
    
        public function singleUser(){ // for showing the list data for admin
        
        try{
//            $user_id = $_SESSION['userJoin']['user_id'];
                    
          $joinQuery = "SELECT * FROM `registration` LEFT JOIN `profile` ON `registration`.`id` = `profile`.`user_id` ";
                            $stmt = $this->conn->prepare($joinQuery);
                            $stmt ->execute();
                            $SessionData = $stmt->fetch(PDO::FETCH_ASSOC);
                            $_SESSION['joinDb'] = $SessionData;
                            
            // *****************  *******************\\
            
//            $query = "SELECT * FROM `profile` WHERE user_id = $this->id";
//            
//            $query = "SELECT * FROM `profile` LEFT JOIN `registration` ON `profile`.`user_id` = `registration`.`id` WHERE unique_id = $this->id";
            

            $query = "SELECT * FROM `profile` LEFT JOIN `registration` ON `profile`.`user_id` = `registration`.`id`  WHERE unique_id ='".$this->id."'";
            $stmt = $this->conn->prepare($query);
            $stmt ->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            
            return $row;
            
        } catch (Exception $ex) {
            echo 'ERROR: ' . $ex->getMessage();
        }
    
       
    }// singleuser \\
    
    
    
    
    public function trash(){
        
        $unique_id = " '$this->id' ";
        
        $query = "UPDATE `registration` SET `is_deleted` = '1' WHERE `registration`.`unique_id` = $unique_id;";
       $stmt = $this->conn->prepare($query);
       if($stmt ->execute()){
           $_SESSION['listSuccess'] = '<p class="btn btn-success btn-lg btn-block">succesfully Deleted</a>';
           header('location:list.php');
       }
    }// trash \\
    
    
    
    
    
    
    public function restore(){
        
        $unique_id = " '$this->id' ";
        
        $query = "UPDATE `registration` SET `is_deleted` = '0' WHERE `registration`.`unique_id` = $unique_id;";
       $stmt = $this->conn->prepare($query);
       if($stmt ->execute()){
           $_SESSION['listSuccess'] = '<p class="btn btn-success btn-lg btn-block">succesfully Restored</a>';
           header('location:list.php');
       } // execute
    }// restore \\
    
    
    
    
    
    public function delete(){
        
        $unique_id = " '$this->id' ";
        
        $query = "DELETE FROM `registration` WHERE `registration`.`unique_id` = $unique_id;";
       $stmt = $this->conn->prepare($query);
       if($stmt ->execute()){
           $_SESSION['deleteSuccess'] ='<p class="btn btn-danger btn-lg btn-block">succesfully Deleted</a>';
           header('location:trashlist.php');
       } // execute
    }// delete \\
    
    
      
    
    
    
    
    public function update(){
        try{
                
             $queryProfile  = "UPDATE profile SET full_name  = :full_name ,  father_name  = :father_name ,  mother_name  = :mother_name ,  gender  = :gender ,  birthdate  = :birthdate ,  occupation  = :occupation ,  education  = :education ,  religion  = :religion ,  marritial_stat  = :marritial_stat ,  mobile  = :mobile ,  job_status  = :job_status ,  Nationality  = :Nationality ,  interest  = :interest,  bio  = :bio ,  nid  = :nid ,  passpoet  = :passpoet ,  skill_arel  = :skill_arel ,  languages  = :languages ,  profile_pic  = :profile_pic ,  blood  = :blood ,  height  = :height ,  fax  = :fax ,  address  = :address ,  other  = :other ,  modified  = :modified WHERE user_id = :user_id ";

            $stmt = $this->conn ->prepare($queryProfile);
                            
            $stmt ->execute(array(
                 ":full_name" =>$this->name,
                 ":father_name" =>$this->father,
                 ":mother_name" =>$this->mother,
                 ":gender" =>$this->gender,
                ":birthdate" =>  serialize($this->birthday),
                ":occupation" =>$this->occupation,
                 ":education" =>$this->education,
                 ":religion" =>$this->religion,
                 ":marritial_stat" =>$this->married,
                 ":mobile" =>$this->mobile,
                 ":job_status" =>$this->job,
                 ":Nationality" =>$this->nation,
                 ":interest" =>  serialize($this->interest), 
                 ":bio" =>$this->bio,
                 ":nid" =>$this->nid,
                 ":passpoet" =>$this->passport,
                 ":skill_arel" =>$this->skill,
                 ":languages" =>  serialize($this->language),
                 ":profile_pic" =>$this->pic,
                 ":blood" =>$this->blood,
                 ":height" =>$this->height,
                 ":fax" =>$this->fax,
                 ":address" =>  serialize($this->address),
                 ":other" =>$this->other,
                 ":modified" =>date("Y-m-d h:i:s"),
                 ":user_id" => $_SESSION['user']['id'],
                
                ));
                header("location:edit.php");
                $_SESSION['status'] = '<p class="text-danger">Thank you for Update</p>';
       
            } catch(PDOException $ex){
                echo "Error: ". $ex->getMessage();
                $_SESSION['status'] = '<p class="text-danger">Update Failed, Please try again</p>';
            }// catch
           
           
    }// Update \\
    
    
    
    
    
    
    
    
} // class \\

