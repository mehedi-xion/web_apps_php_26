<?php

require_once '../../../../src/bitm/seip131181/semister/Semister.php';

use SemisterApp\bitm\seip131181\semister\Semister;

$objEdit = new Semister();

$data = $objEdit -> prepare($_GET) -> show() ;

?>
<html>
       <head>
              <title>Edit</title>
              <link href="../../../../../css/bootstrap.min.css" rel="stylesheet">
       </head>
       <body>
           
              <div class="container">
                     <fieldset>
                            <legend>Edit and Update</legend>
                            
                             <?php
                                if(isset($_SESSION["Message"]) && !empty($_SESSION["Message"])){
                                    echo $_SESSION["Message"] ;
                                    unset($_SESSION["Message"]);
                                }
                            
                            ?>
                            
                            <form class="form-inline" method="POST" action="update.php">
                                   
                                   
                                    <div class="form-group">
                                        <label >Your Name:</label>
                                        <input type="text" class="form-control" name="fname" value = "<?php echo $data['name']; ?>">
                                    </div>
                                   
                                    <div class="form-group">
                                            <label >Select Semister:</label>
                                                <select class="form-control" name = "semister">
                                                     <option value="1st_Sem">1st Sem</option>
                                                    <option value="2nd_Sem">2nd Sem</option>
                                                    <option value="3rd_Sem">3rd Sem</option>
                                                </select>
                                  </div>
                                   
                                   
                                    <div class="checkbox">
                                        <label><input type="checkbox" name="offer" value="Offered"  <?php if($data['offer']=='Offered'){
     echo "checked";
 } ?>>Get Offer</label>
                                           <input type="text" class="form-control" name="id" value = "<?php echo $data['unique_id']; ?>">
                                    </div>
                                   
                                   
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </form>
                           </fieldset>
                     <p><a href="index.php">Show List</a></p>
              </div>  <!--container -->
              
       
       </body>
</html>

