<?php

require_once '../../../../src/bitm/seip131181/semister/Semister.php';

use SemisterApp\bitm\seip131181\semister\Semister;

$objShow = new Semister();

$singleData = $objShow -> prepare($_GET) -> show();

?>
<div class="container">
  <h2>Details view</h2>            
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Semister</th>
        <th>Offer</th>
        <th>Cost</th>
        <th>Waiver</th>
        <th>Total</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><?php echo $singleData['id'] ?></td>
        <td><?php echo $singleData['name'] ?></td>
        <td><?php echo $singleData['semister'] ?></td>
        <td><?php echo $singleData['offer'] ?></td>
        <td><?php echo $singleData['cost'] ?></td>
        <td><?php echo $singleData['waiver'] ?></td>
        <td><?php echo $singleData['total'] ?></td>

      </tr>

    </tbody>
  </table>
  <a href="store.php">Show List</a>
</div>

       </body>
</html>
