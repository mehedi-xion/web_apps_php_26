<?php

require_once '../../../../src/bitm/seip131181/semister/Semister.php';

use SemisterApp\bitm\seip131181\semister\Semister;

$objStore = new Semister();

$objStore -> prepare($_POST) -> store();