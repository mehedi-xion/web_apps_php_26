<?php
error_reporting(E_ALL);
error_reporting(E_ALL & ~E_DEPRECATED);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors',TRUE);
date_default_timezone_set('Europe/London');

if(PHP_SAPI == 'cli')
    die('This example should only be run from a Browser');

// Include PHPExel

require_once '../../../../vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
require_once '../../../../vendor/autoload.php';

use MobileApp\Bitm\Seip10\Mobile\Mobile;
 
 
$objExcel = new Mobile();

$Alldata = $objExcel ->index();

// Create new phpExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel ->getProperties()->setCreator("Maarten Balliauw")
                        ->setLastModifiedBy("Maarten")
                         ->setTitle("Office 2007")
                         ->setSubject("Office 2007")
                         ->setDescription("Testing")
                         ->setKeywords("Office 2007")
                         ->setCategory("test result file");

//Add some Data

$objPHPExcel ->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'SL')
                         ->setCellValue('B1', 'ID')
                         ->setCellValue('C1', 'Mobile Model')
                         ->setCellValue('D1', 'Laptop Model');

$counter = 2;
$serial = 0;

foreach($Alldata as $data){
    $serial++;
    $objPHPExcel ->setActiveSheetIndex(0)
                ->setCellValue('A'.$counter, $serial)
                ->setCellValue('B'.$counter, $data['id'])
                ->setCellValue('C'.$counter, $data['Mobile'])
                ->setCellValue('D'.$counter, $data['laptop']);
    $counter++;
}

// Rename Worksheet

$objPHPExcel->getActiveSheet()->setTitle('Mobile_list');

// Set Active sheet index to the first sheet, so Excel  open this as the first sheet
$objPHPExcel ->setActiveSheetIndex(0);

// Redirect output to a clients web broser
header('countent-type: application/vnd.ms-excel');
header('Content-disposition: attachment;filename= "01simple.xls"');
header('Cache-Control: max-age=0');


$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter -> save('php://output');
exit;