<?php
namespace MobileApp\Bitm\Seip10\Mobile;

use PDO;

class Mobile
{
    public $id = '';
    public $title = '';
    public $data = '';
    public $laptop = "";
    public $dbuser = "root";
    public $dbpass = "";
    public $con = "";
    
    
    public function __construct()
    {
        session_start();
        $this ->con = new PDO('mysql:host=localhost;dbname=web_apps_php_26', $this->dbuser, $this->dbpass);
            }

    public function  prepare($data = '')
    {
        if (array_key_exists('mobile_model', $data) && !empty($data['mobile_model'])) {
            $this->title = $data['mobile_model'];
        }else{
            $_SESSION['mobErr'] = "Please insert Mobile Model";
        }
        
        if (array_key_exists('laptop', $data)&& !empty($data['laptop'])) {
            $this->laptop = $data['laptop'];
        }else{
            $_SESSION['lapErr'] = "Please insert Laptop Brand or  Model";
        }
        

        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        $_SESSION['Alldata']=$data;

        
        return $this;
    }
    public function store(){
            if(!empty($this->title) &&  !empty($this->laptop)){
                
                
          try{ 
              $query = "INSERT INTO mobile (`id`, `Mobile`, `laptop`, `unique_id`) VALUES (:i, :m, :l, :u)";
               
              $stmt = $this->con->prepare($query);
              $stmt ->execute(array(
                 ":i" =>null,
                  ":m" => $this->title,
                  ":l" => $this->laptop,
                  ":u" => uniqid(),          
              ));
              header('location:index.php');
            }catch(PDOException $e){
                echo "ERROR" . $e->getMessage();
            } // catch \\
    
    } // validation if \\
    
    } // store \\

    public function index()
    {
        $qr = "SELECT * FROM `mobile`";
       $query = $this->con ->prepare($qr);
       $query ->execute();
        while ($row = $query ->fetch(PDO::FETCH_ASSOC)) {
            $this->data[] = $row;
        }
        return $this->data;
    }

    public function show()
    {
        $query = "SELECT * FROM `mobile` WHERE unique_id='". $this->id."'";
        
        $mydata = $this->con->prepare($query);
        $mydata ->execute();
        $row = $mydata->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public function update()
    {
        $query = "UPDATE mobile SET Mobile = :m, laptop = :l WHERE unique_id = :u";
            
      //$query = "INSERT INTO mobile (`id`, `Mobile`, `laptop`, `unique_id`) VALUES (:m, :l, :u)";
               
              $stmt = $this->con->prepare($query);
              $stmt ->execute(array(
                  ":m" => $this->title,
                  ":l" => $this->laptop,
                  ":u" => $this->id,      
              ));
        
        if (mysql_query($query)) {
            $_SESSION['Message'] = "Data Successfully Updated";
        }
        header("location:edit.php?id=$this->id");

    }

    public function delete()
    {
        $query = "DELETE FROM `mobile` WHERE `mobile`.`unique_id` ='". $this->id."'";
       $mydata = $this->con->prepare($query);
        $mydata ->execute();
          
        if (mysql_query($query)) {
            $_SESSION['Message'] = "Data Successfully Deleted";
        }
        header("location:index.php");
    }
}











