<?php

$color = array('white', 'green', 'red', 'blue', 'black');
		
echo "The memory of that scene for me is like a frame of film forever frozen at that Moment: the ".$color[2]." carpet, the ".$color[1]." lawn, the ".$color[0]." house, the leaden sky. The new President and his first lady. - Richard M. Nixon";
