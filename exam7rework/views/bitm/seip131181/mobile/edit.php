<title>Edit</title>

<?php 

require_once '../../../../src/bitm/seip131181/mobile/Semister.php';

use SemisterApp\bitm\seip131181\mobile\Semister ;

$objEdit = new Semister;

$data = $objEdit -> prepare($_GET) -> show();

?>
<div class="container">

<?php


if(isset($_SESSION['UpdtMsg']) && !empty($_SESSION['UpdtMsg'])){
	echo "<div class='alert alert-success'>
  <strong>Success!</strong>".$_SESSION['UpdtMsg']."
</div>";
	unset($_SESSION['UpdtMsg']);
}

?>

  <fieldset class="form-group">
    <legend>Edit your Info </legend>
		<form class="form-inline" method="POST" action="update.php">
			  <div class="form-group">
			    <label for="exampleInputName2">Enter Your name</label>
			    <input type="text" name="fname" class="form-control" value="<?php echo $data['name'];?>">
			    <input type="hidden" name="id" class="form-control" value="<?php echo $data['unique_id'];?>">
			  </div>
			  <!-- Enter Your Name -->

			   <div class="form-group">
				  <select class="form-control" name="semister">
				    <option value="1st Semister" <?php if($data['semister'] =='1st Semister'){echo 'selected';} ?>>1st Semister</option>
					<option value="2nd Semister" <?php if($data['semister'] =='2nd Semister'){echo 'selected';} ?>>2nd Semister</option>
					<option value="3rd Semister" <?php if($data['semister'] =='3rd Semister'){echo 'selected';} ?>>3rd Semister</option>
				  </select>
				</div>
			  <!-- Semister DropDown list -->

			  <div class="checkbox">
		        <label>
		          <input type="checkbox" name="offer" value="offered" <?php if($data['offer'] =='offered'){echo 'checked';} ?>> For Offer
		        </label>
		      </div>
		      <!-- Checkbox as Offer -->

			  <button type="submit" class="btn btn-primary">Update</button>
			  <!-- Submit -->

		</form>
</fieldset>
<p><a style="text-decoration: none; color:black" href="index.php">Go to list</a></p>

</div>
</body>
</html>