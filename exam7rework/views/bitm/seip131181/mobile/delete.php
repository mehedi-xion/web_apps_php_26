<title>Delete</title>

<?php 

require_once '../../../../src/bitm/seip131181/mobile/Semister.php';

use SemisterApp\bitm\seip131181\mobile\Semister ;

$objDelete = new Semister;

$objDelete -> prepare($_GET) -> delete();