<title>Index</title>

<?php 

require_once '../../../../src/bitm/seip131181/mobile/Semister.php';

use SemisterApp\bitm\seip131181\mobile\Semister ;

$objIndex = new Semister;

$AllData = $objIndex -> index();

$serial = 1;
?>

</head>
<body>

<div class="container">
       
<?php


if(isset($_SESSION['DeletMsg']) && !empty($_SESSION['DeletMsg'])){
	echo "<div class='alert alert-success'>
  <strong>Success!</strong>".$_SESSION['UpdtMsg']."
</div>";
	unset($_SESSION['UpdtMsg']);
}

?>

  <h2>All Data In List View</h2>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Semister</th>
        <th>Offer</th>
        <th>Cost</th>
        <th>Waiver</th>
        <th>Total</th>
        <th>Unique Id</th>
        <th colspan="3">Action</th>
      </tr>
    </thead>
    <tbody>
    <?php 
    	foreach($AllData as $SingleData){ ?>    	
      <tr>
        <td><?php echo $serial++; ?></td>
        <td><?php echo $SingleData['name']; ?></td>
        <td><?php echo $SingleData['semister']; ?></td>
        <td><?php echo $SingleData['offer']; ?></td>
        <td><?php echo $SingleData['cost']; ?></td>
        <td><?php echo $SingleData['waiver']; ?></td>
        <td><?php echo $SingleData['total']; ?></td>
        <td><?php echo $SingleData['unique_id']; ?></td>
        <td><a href="show.php?id=<?php echo $SingleData['unique_id'];?>">View</td>
        <td><a href="edit.php?id=<?php echo $SingleData['unique_id'];?>">Edit</td>
        <td><a href="delete.php?id=<?php echo $SingleData['unique_id'];?>">Delete</td>
      </tr>
        	<?php

		  	}
		  	
		  	?>
    </tbody>
  </table>
  	<a href="creat.php">Add New Item</a>
</div>

</body>
</html>

